from django.urls import path

from . import views

app_name = 'story5'

urlpatterns = [
    path('', views.inputhtml, name='inputhtml'),
    path('output',views.outputhtml,name='outputhtml'),
]