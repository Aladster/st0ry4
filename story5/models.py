from django.db import models

# Create your models here.

from django.utils import timezone
from datetime import datetime, date


class Jadwal(models.Model):
    nama_mata_kuliah = models.CharField(max_length=50)
    dosen_pengajar = models.CharField(max_length=50)
    jumlah_sks = models.CharField(max_length=5)
    deskripsi_mata_kuliah=models.CharField(max_length=150)
    semester_tahun=models.CharField(max_length=20)
    ruang_kelas=models.CharField(max_length=50)


class Post(models.Model):
    Kuliah = models.ForeignKey(Jadwal,null=True, on_delete =models.CASCADE)
    content = models.CharField(max_length=125)
    published_date = models.DateTimeField(default=timezone.now)
    


        