from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Input_Form
from .models import Jadwal,Post

# Create your views here.
def inputhtml(request):
    form = Input_Form(request.POST or None)
    if (form.is_valid and request.method =="POST"):
       form.save()
       return HttpResponseRedirect('')
    else:
       return render(request,'input.html',{'form':form})

def outputhtml(request):
    jadwal = Jadwal.objects.all()
    response = {'jadwal':jadwal}
    html = 'output.html'
    return render(request, html, response)
