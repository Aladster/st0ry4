from django import forms
from .models import Jadwal

    
class Input_Form(forms.ModelForm):
    class Meta:
        model = Jadwal
        fields = ['nama_mata_kuliah','dosen_pengajar','jumlah_sks','deskripsi_mata_kuliah','semester_tahun','ruang_kelas']
        
    error_messages = {
        'required' : 'Please Type'
    }
    input_attrs = {
        'type' : 'text',
    }  
    nama_mata_kuliah = forms.CharField(label='nama mata kuliah ', required=True, max_length=50, widget=forms.TextInput(attrs=input_attrs))
    dosen_pengajar = forms.CharField(label='nama dosen ', required=True, max_length=50, widget=forms.TextInput(attrs=input_attrs))
    jumlah_sks = forms.CharField(label='jumlah sks ', required=True, max_length=5, widget=forms.TextInput(attrs=input_attrs))
    deskripsi_mata_kuliah = forms.CharField(label='deskripsi mata kuliah ', required=True, max_length=150, widget=forms.TextInput(attrs=input_attrs))
    semester_tahun = forms.CharField(label='semester tahun ', required=True, max_length=20, widget=forms.TextInput(attrs=input_attrs))
    ruang_kelas = forms.CharField(label='ruang kelas ', required=True, max_length=50, widget=forms.TextInput(attrs=input_attrs))
    

