from django.contrib import admin

# Register your models here.
from .models import Jadwal, Post
admin.site.register(Jadwal)
admin.site.register(Post)
