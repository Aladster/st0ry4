from django.urls import path

from . import views

app_name = 'main'

urlpatterns = [
    path('', views.home, name='home'),
    path('story1/', views.story1, name='story1'),
    path('story3/', views.story3, name='story3'),
    path('miripstory3/', views.miripstory3, name='miripstory3'),
]
